package com.ws.study.ex3;

import lombok.Data;

/**
 * @Author Administrator
 * @Date 2023/12/4 19:53
 * @Description
 **/
@Data
public class HelloMessage {
    private String name;
    private String userId;

}
