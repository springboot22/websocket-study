package com.ws.study.ex3;

import lombok.Data;

/**
 * @Author Administrator
 * @Date 2023/12/4 19:53
 * @Description
 **/
@Data
public class Greeting {
    private String content;

    public Greeting() {
    }

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
