package com.ws.study.ex3;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @Author Administrator
 * @Date 2023/12/4 13:50
 * @Description STOMP 协议方式
 **/

/**
 * @EnableWebSocketMessageBroker 启用Websocket 消息处理
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketSTOMPConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 配置相关的 WebSocket 消息代理
     * @param registry
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 订阅的broker 名称： topic 代表广播即群发
        // queue 代表点对点，即发给指定用户
        registry.enableSimpleBroker("/topic");
        // 配置全局应用程序订阅前缀
        // send 命令时 需要带上 /app 前缀
        registry.setApplicationDestinationPrefixes("/app");
        //修改convertAndSendToUser方法前缀
        // 点对点使用的订阅前缴（客户端订阅路径上会体现出来）
        // 不设置的话，默认也是/user
//        registry.setUserDestinationPrefix ("/user/");
    }

    // 注册 WebSocket 端点并映射到指定的URl
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/gs-guide-websocket");
    }

    /**
     * 处理断连
     * @param registration
     */
    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
//        registration.interceptors(myStompChannelInterceptor());
    }

}
