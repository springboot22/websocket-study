package com.ws.study.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

/**
 * @Author Administrator
 * @Date 2023/12/4 19:22
 * @Description
 **/
@Slf4j
@Controller
public class CaseController {

    /**
     * @MessageMapping：功能与RequestMapping注解类似。send指令发送信息时添加此注解。
     * @SendTo/@SendToUser：将信息输出到该主题。客户端订阅同样的主题后就会收到信息。 在只有指定@MessageMapping时@MessageMapping == “/topic” + @SendTo。
     * <p>
     * 如果想使用rest接口发送消息。可以通过SimpMessagingTemplate进行发送。
     * <p>
     * 点对点聊天时，可以使用SimpMessagingTemplate.convertAndSendToUser方法发送。个人意味比注解@SendToUser更加容易理解，更加方便。
     * <p>
     * convertAndSendToUser方法和convertAndSend类似，区别在于convertAndSendToUser方法会在主题默认添加/user/为前缀。因此，示例代码中convertAndSend方法直接传入"/topic/user/“+message.getUserId
     * ()+”/sendToUser" 也是点对点发送。topic其中是默认前缀。
     * <p>
     * 如果想修改convertAndSendToUser默认前缀可在配置类进行配置，可在WebSocketConfig类中查看。
     */

    /**
     * SimpMessagingTemplate的作用 1、SimpMessagingTemplate可以在应用的任意地方发送消息。
     * Spring的SimpMessagingTemplate能够在应用的任何地方发送消息，甚至不必以首先接收一条消息作为前提。使用SimpMessagingTemplate的最简单方式是将它（或者其接口SimpMessageSendingOperations）自动装配到所需的对象中。
     * <p>
     * 2、SimpMessagingTemplate可以为指定的用户发送消息。
     * SimpMessagingTemplate还提供了convertAndSendToUser()方法。convertAndSendToUser()方法能够让我们给特定用户发送消息。
     * <p>
     * <p>
     * <p>
     * <p>
     * 1. simpMessageSendingOperations.convertAndSendToUser("1", "/message", "测试convertAndSendToUser"); 2. 3.
     * stomp.subscribe('/users/1/message', function(message){ });
     * 客户端接收一对一消息的主题是"/users/“+usersId+”/message"，这里的用户Id可以是一个普通字符串，只要每个客户端都使用自己的Id并且服务器端知道每个用户的Id就行了。
     */
//    @Autowired
//    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * 利用@SendTo 注解，使方法的返回值推送到消息代理器中，由消息代理器广播到订阅路径中去。但并没有详细的介绍消息是怎样被Spring框架处理，最后发送广播出去的。
     * 使用SendTo注解来标识这个方法返回的结果，都会被发送到它指定的destination，“/topic/greetings”. 传入的参数Message为客户端发送过来的消息，是自动绑定的。
     * <p>
     * 方法中的返回值，会被广播到/topic/greetings 这个订阅路径中，只要客户端订阅了这个路径，都会接收到消息。 Spring处理消息的主要类是SimpleBrokerMessageHandler,
     * 当需要发送广播消息时，最终会调用其中的sendMessageToSubscribers()方法：方法内部会循环调用当前所有订阅此 Broker 的客户端 Session ，然后逐个发送消息。这里，入参 destination 就是 Broker 的地址，而 message，就是我们返回信息的封装，其他细节这里就不展开讲了。
     * 使用MessageMapping注解来标识所有发送到“/hello”这个destination的消息，都会被路由到这个方法进行处理.
     *
     * @param message
     * @return
     * @throws Exception
     */
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

//    @MessageMapping("/topic/greetings")
//    public Greeting greeting2(HelloMessage message) throws Exception {
//        Thread.sleep(1000); // simulated delay
//        log.info("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
//        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
//    }
//
//    @GetMapping("/hello2")
//    public void greeting3(HelloMessage message) throws Exception {
//        Thread.sleep(1000); // simulated delay
//        simpMessagingTemplate.convertAndSend("/topic/greetings",
//                new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"));
//    }
//
//
//    @MessageMapping("/sendToUser")
//    public void sendToUser(HelloMessage message) throws Exception {
//        Thread.sleep(1000); // simulated delay
//        log.info("userId:{},msg:{}", message.getUserId(), message.getName());
////      simpMessagingTemplate.convertAndSendToUser (message.getUserId (),"/sendToUser",
////              new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"));
////      simpMessagingTemplate.convertAndSend ("/user/1/sendToUser",
////              new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"));
//        simpMessagingTemplate.convertAndSend("/topic/user/" + message.getUserId() + "/sendToUser",
//                new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"));
//    }

    /**
     * 那么如果我只是想用WebSocket向服务器发出查询请求，然后服务器你就把查询结果给我就行了，其他用户就不用你广播推送了，简单点，就是我请求，你就推送给我。这又该怎么办呢？是的， @SendToUser 就能解决这个问题。
     */

//    @MessageMapping("/hello")
//    //使用MessageMapping注解来标识所有发送到“/hello”这个destination的消息，都会被路由到这个方法进行处理.
//    @SendToUser("/topic/greetings")
//    //使用SendToUser注解来标识这个方法返回的结果，都会被发送到请求它的用户的destination.
//    //传入的参数Message为客户端发送过来的消息，是自动绑定的。
//    public Greeting greetingA(HelloMessage message) throws Exception {
//        Thread.sleep(1000); // 模拟处理延时
//        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"); //根据传入的信息，返回一个欢迎消息.
//    }
}
