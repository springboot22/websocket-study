package com.ws.study.ex2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * @Author Administrator
 * @Date 2023/12/4 11:11
 * @Description
 **/
@Slf4j
public class SimpleServerV2 implements WebSocketHandler {

    /**
     * 在建立连接后的处理逻辑
     * @param webSocketSession
     * @throws Exception
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        log.info("afterConnectionEstablished");
    }

    /**
     * 处理接收到的消息
     * @param webSocketSession
     * @param webSocketMessage
     * @throws Exception
     */
    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage)
            throws Exception {
        log.info("handleMessage:{}",webSocketMessage);
    }

    /**
     * 处理传输错误
     * @param webSocketSession
     * @param throwable
     * @throws Exception
     */
    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        log.error("handleMessage:",throwable);
    }
    /**
     * 在连接关闭后的处理逻辑
     * @return
     */
    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        log.info("handleMessage:{}",closeStatus);
    }


    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
