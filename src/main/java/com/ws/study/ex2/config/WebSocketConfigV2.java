package com.ws.study.ex2.config;

import com.ws.study.ex2.SimpleServerV2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

/**
 * @Author Administrator
 * @Date 2023/12/4 11:10
 * @Description
 **/
//@Configuration
//@EnableWebSocket
public class WebSocketConfigV2 implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        // 我们创建了一个新的WebSocketServer对象，并将其添加到WebSocket处理程序中。我们还指定了WebSocket端点（/websocket）和允许的来源（*）。
        registry
                .addHandler(myHandler(), "/myHandler")
                .setAllowedOrigins("*");
    }

    @Bean
    public WebSocketHandler myHandler() {
        return new SimpleServerV2();
    }
}