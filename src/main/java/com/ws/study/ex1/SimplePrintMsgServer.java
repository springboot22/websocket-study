package com.ws.study.ex1;

import java.io.IOException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.springframework.stereotype.Component;

/**
 * @Author Administrator
 * @Date 2023/12/4 10:37
 * @Description 一个简单的 打印消息的示例
 **/
//@Component
//@ServerEndpoint("/echo")
public class SimplePrintMsgServer {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket 连接已经建立。");
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        System.out.println("收到客户端消息：" + message);
        session.getBasicRemote().sendText("服务器收到消息：" + message);
    }

    @OnClose
    public void onClose() {
        System.out.println("WebSocket 连接已经关闭。");
    }

    @OnError
    public void onError(Throwable t) {
        System.out.println("WebSocket 连接出现错误：" + t.getMessage());
    }
}

